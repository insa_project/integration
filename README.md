Auteur: BLAGNY Julien
	
Configuration matérielle:
	- Ordinateur MANJARO
	- Connexion  wifi
------------------------------------------------------------------------
	
Cette application permet de stocker et recevoir des informations recu par un 
capteur. Les données traitées, permetttent à l'utilisateur de savoir 
quel est la force et la direction du vent à l'endroit ou se situe le
capteur.

Afin de rendre plus rapide et plus simple sont implémentation, ce 
dossier contient un installateur automatique. 
Deux choix s'offre à vous, configurer le serveur grace à cet installateur
ou le faire à la main.

Ansible est un outils capable de gérer plusieur serveurs. Ici, Ansible
permet de:
	- Installer les dépendances sur la Raspberry Pi
	- Configurer les dépendances
	- Déployer l'application servernode

Ce dossier contient:
	- Un script ./config, permettant de configurer sa machine et exécuter 
	tous les roles Ansible
	- Un script ./install, permettant d'exécuter tous les roles Ansible
	- Un script ./deploy, permettant de deployer son application via
	 Ansible
	- Un script ./start, permettant de démarrer les containers
	 
	- Un dossier Ansible, permettant d'installer, configurer et déployer
	l'application sur la Raspberry Pi
	- Un dossier openvisio, contenant toute l'application qui sera placée sur la 
	Raspberry Pi. Le script de déploiement s'occupe de la copie.

------------------------------------------------------------------------

La configuration se déroulera en deux étapes: 

	- Configuration de votre ordinateur
		- Si absent, création d'une clé ssh
		- Envoi de cette clé au Raspberry Pi
		- Installation de Ansible
		
	- Configuration de la Raspberry Pi
		- Installation des dépendences
		- Configuration de celle-ci
		- Déploimenent de l'application
		
		
Avant toute chose, assurez-vous que:
	- Vous possedez les droits administrateurs
	- Vous etes sur le même réseau que votre Raspberry Pi
	- Que le service SSH y est activé
	- Que vous connaissez son addresse IP
	
	
I - Configuration de votre ordinateur:
 -> Exécutez le script: config:
	- $ sudo ./config
	Ce script vous permettra de configurer votre machine ET la Raspberry Pi

II - Configuration de la Raspberry Pi
 -> Si vous avez déja un ordinateur configuré, vous 
 pouvez exécuter le script ./install
	- $ ./install
	Le script vous guidera afin de configurer correctement Ansible.
	Il vous sera demandé d'ajouter ou de modifier l'adresse ip de la
	Raspberry Pi.
	Il vous faudra vérifier si l'adresse est la bonne et modifier en conséquence.
	Pour sauvegarder le fichier et continuer l'installation:
		- Appuyez sur ctrl+o pour sauvegarder
		- Appuyez sur entré pour valider la sauvegarde
		- Appuyez sur ctrl+x pour quitter

III - Seulement déployer le projet sur la Raspberry Pi
 -> Si les étapes précédentes sont déja faite ou si vous ne souhaitez pas 
 relancer une installation, exécutez ./deploy:
	- $ ./deploy

--> Ce script permet d'archiver le dossier openvisio, et de le déployer 
sur la Raspberry Pi.

le déployement réinitialisera la base de donnée si elles existent
déja sur la Raspberry Pi. En effet une migration est effectuée.
